using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using controlAnimcacion;

public class CargarConfiguracionDelPlayer : MonoBehaviour
{
    [SerializeField]
    private Animator PlayerAnimator;
    [SerializeField]
    private ScriptObjAniSelector SOAnimacionSeleccionada;

    // Start is called before the first frame update
    void Start()
    {
        ejecutarAnimacionSeleccionada();
    }


    private void  ejecutarAnimacionSeleccionada()
    {
        PlayerAnimator.Play(SOAnimacionSeleccionada.GetAnimaionELegida().ToString());
    }

}
