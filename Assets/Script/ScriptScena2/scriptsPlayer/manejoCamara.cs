using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class manejoCamara : MonoBehaviour
{
    [SerializeField]
    private float sensibilidadDElMause = 80f;
    [SerializeField]
    private Transform transformPlayer;

    private float  RotacionX=0f;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        actualizarMovimentoCamara();
    }

    private void actualizarMovimentoCamara()
    {
        float mouseX = Input.GetAxis("Mouse X") * sensibilidadDElMause * Time.deltaTime;

        float mouseY = Input.GetAxis("Mouse Y")* sensibilidadDElMause * Time.deltaTime;

        RotacionX += mouseY;

        RotacionX = Mathf.Clamp(RotacionX,-90f,90f);

        transform.localRotation=  Quaternion.Euler(RotacionX,0,0);

        transformPlayer.Rotate(Vector3.up*mouseX);
    }


}
