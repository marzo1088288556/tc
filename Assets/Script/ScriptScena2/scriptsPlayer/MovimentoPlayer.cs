using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimentoPlayer : MonoBehaviour
{
    [SerializeField]
    private float Velocidad;

    [SerializeField]
    private CharacterController controladorDeColicion;

    private float gravedad = -9.81f;

    [SerializeField]
    private Transform tierra;
    [SerializeField]
    private float radioEsfera = 0.3f;
    private Vector3 velocidadY;
    [SerializeField]
    private LayerMask masCaraTierra;

    private bool entierra;
    private void Start()
    {
        controladorDeColicion = GetComponent<CharacterController>();
    }

    void Update()
    {
        caminaMandoWASD();
    }

    private void FixedUpdate()
    {
       
    }

    private void caminaMandoWASD()
    {
        float ejeX = Input.GetAxis("Horizontal");
        float ejeZ = Input.GetAxis("Vertical");
        estoyEnTierra();
        Vector3 movimento = transform.right * ejeX + transform.forward * ejeZ;

        controladorDeColicion.Move(movimento*Velocidad*Time.deltaTime);
        agregarGravedadSimple();
    }


    private void agregarGravedadSimple()
    {
        velocidadY.y += gravedad*Time.deltaTime;

        controladorDeColicion.Move(velocidadY*Time.deltaTime);

    }

    private void estoyEnTierra()
    {
        entierra = Physics.CheckSphere(tierra.position,radioEsfera,masCaraTierra);
        if (entierra&& velocidadY.y<0)
        {
            velocidadY.y = -2f;
        }
    }

}
