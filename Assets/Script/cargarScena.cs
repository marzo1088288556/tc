using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class cargarScena : MonoBehaviour
{
    public void cargarSiguienteScena(int idDeLaEscena)
    {
        SceneManager.LoadScene(idDeLaEscena);
    }
}
