using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using controlAnimcacion;

public class CambiarAni : MonoBehaviour
{
    [SerializeField]
    private Animator AnimatorPlayer;

    [SerializeField]
    private ScriptObjAniSelector SOAnimacionSeleccionada;

    


    public void AniHouseDancing()
    {
        AnimatorPlayer.Play("HouseDancing");
        SOAnimacionSeleccionada.setAnimacion(animacionElegida.HouseDancing);
    }


    public void AniMacarena()
    {
        AnimatorPlayer.Play("Macarena");
        SOAnimacionSeleccionada.setAnimacion(animacionElegida.Macarena);
    }

    public void AniHipHopDance()
    {
        AnimatorPlayer.Play("HipHopDance");
        SOAnimacionSeleccionada.setAnimacion(animacionElegida.HipHopDance);
    }

}
