using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace controlAnimcacion
{

    [CreateAssetMenu]
    public class ScriptObjAniSelector : ScriptableObject
    {
        [SerializeField]
        private animacionElegida AnimacionElegida = animacionElegida.ninguna;



        public void setAnimacion(animacionElegida aniSeleccionada)
        {
            AnimacionElegida = aniSeleccionada;
        }


        public animacionElegida GetAnimaionELegida()
        {
            return AnimacionElegida;
        }

    }
    public enum animacionElegida
    {
        ninguna,
        HouseDancing,
        Macarena,
        HipHopDance
    }
}
